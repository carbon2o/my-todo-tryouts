import React from 'react';
import './todo.css';

class Todo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue : this.props.todo.text,
            isEditMode : false,
        };
      }

    onDoubleClick() {
        this.setState( {isEditMode: true} );
    }

    handleKeyPress(event) {
            if(event.key === 'Enter'){
                console.log(event);
                console.log(this.state.inputValue);
                const editedTodo = {...this.props.todo, text : this.state.inputValue};
                this.props.handleEdit(editedTodo);
                this.setState( {isEditMode: false} );
            }
      }

      onEditChange(event) {
          this.setState( {inputValue: event.value });
      }

    render () {
        let displayMode;
        if (!this.state.isEditMode) {
            displayMode = <label onDoubleClick={this.onDoubleClick.bind(this)}>{this.props.todo.text}</label>;
         } else {
            displayMode =  <input type="text" defaultValue={this.state.inputValue} onChange={this.onEditChange.bind(this)} onKeyPress={this.handleKeyPress.bind(this)}/>
         }

        return (
                <div class="todo-item">
                    {this.props.todo.order + " - "}

                    {displayMode}

                    <input class="btn-todo-check" type="checkbox" defaultChecked={this.props.todo.isDone}
                    onChange={() => this.props.handleCheck(this.props.todo)}
                    />
                    <button class="btn-todo-delete" onClick={() => this.props.handleDelete(this.props.todo)}>Del</button>
                </div>
            );

    }

}

class TodoList extends React.Component {
    constructor() {
        super();
        this.state = {
            todos: [
              { order:1, text: "Learn about React", isDone: false },
              { order:2, text: "Meet friend for lunch", isDone: false },
              { order:3, text: "Build really cool todo app", isDone: true },
              { order:4, text: "I should've done better :", isDone: true }
            ]
          }
    }

    handleAdd() {
        const tempTodos = this.state.todos.slice();
        tempTodos.push({order: tempTodos.length + 1, text: '', isDone: false});
        this.setState({
            todos: tempTodos,
        });
    }

    handleDelete(event) {
        const tempTodos = this.state.todos.slice();
        this.setState({
            todos: tempTodos.filter((j) => {return event !== j}),
        });

    }

    handleEdit(todo) {
        const tempTodos = this.state.todos.slice();
        console.log(tempTodos);
        console.log(todo);

        tempTodos[tempTodos.findIndex(t => t.order === todo.order)] = todo;
        console.log(tempTodos);

        // commented because causing an error
        // this.setState({
        //     todos: tempTodos,
        // });
    }

    handleCheck(event) {
        const tempTodos = this.state.todos.slice();

        tempTodos.map(td => {
            if (td.order === event.order) {
                td.isDone = event.checked;
                return tempTodos;
            }
            return tempTodos;
            });

        this.setState({
            todos: tempTodos,
        })
    }
    
    render() {
        return (
            <div>
                <header className="todo-header">
                    {"Trainee To-DO App"}
                </header>
                <div class="todo-col">
                        {this.state.todos.map(item => (
                            <Todo todo={item}
                            handleEdit={this.handleEdit.bind(this)}
                            handleCheck={this.handleCheck.bind(this)}
                            handleDelete={this.handleDelete.bind(this)}
                            />
                        ))}
                        <button class="btn-todo-add" onClick={this.handleAdd.bind(this)}>+</button>
                </div>
            </div>
        );
    }

}

export default TodoList;